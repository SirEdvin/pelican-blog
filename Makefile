shell:
	docker run --rm -it --net host -v ${PWD}:/pelican/base siredvin/pelican-utils:0.3.1 bash
build:
	docker run --rm -it --net host -v ${PWD}:/pelican/base siredvin/pelican-utils:0.3.1 pelican
publish:
	docker run --rm -it --net host -v ${PWD}:/pelican/base siredvin/pelican-utils:0.3.1 pelican -s publishconf.py
serve:
	docker run --rm -it --net host -v ${PWD}:/pelican/base siredvin/pelican-utils:0.3.1 python3 -m pelican.server