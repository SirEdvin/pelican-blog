Title: Цікаві знахідки та результати #1
Slug: tricks-1
Lang: uk
Date: 08-11-2015
Category: Програмування
Tags: python, regex, java
Status: published

###Пошук методу сі-подібної мови за допомогою регулярних виразів
Спробувати знайти метод в коді лише за допомогою одного регексу трошки важкувато. Головна проблема в тому, щоб кількість фігурних дужок у регексі підрахувати неможливо, а без цього складно знайти метод лише за назвою та списком параметрів.

Але, якщо ваш код оформлений за стандартами, то кількість пробільних символів при оголошенні методу та при останній фігурній дужці буде однаковим. Використавши це, можна написати такий простий регекс
```
^(\s+)public static void temp\(DoubleVector<T> d\) \{[\w\W]+^\1\}
```
За допомогою такого регексу легко знаходити такі методи:
```java
    public static void temp(DoubleVector d) {
      if (d !=null){
        d.add(5);
      }
    }
```

**Про прапорці**: цей регекс вимагає прапор Multiline для того, щоб спеціальні символи `^` позначали також початок рядка, а не тільки початок тексту. Замість звичного виразу `.+` був використаний `[\w\W]+`, що означає майже те саме. Різниця в тому, що для того, щоб точка вміщувала в себе пробільні символи потрібний прапор DotAll. Але цей прапор у деяких реалізаціях конфліктує з прапором Multiline (наприклад, у Python).

###Динамічне перетворення типів і його використання у Java 8
*за матеріалами [Casting In Java 8 (And Beyond ?)](http://blog.codefx.org/java/casting-in-java-8-and-beyond/)*

Є звичайний спосіб перетворення типів, який постійно використовується:
```java
Object obj;
if (obj instanceof Integer){
  Integer objInt = (Integer) objl;
}
```

В цьому випадку відразу задається компілятору тип `Integer`. Оскільки компілятор вже на етапі компіляції знає тип, на який буде перетворюватися об’єкт, то такий спосіб перетворення називається *статичний*.

В Java 5 ввели ще один спосіб перетворення типів:
```java
Object obj; // may be an integer
if ( Integer.class.isInstance(obj)) {
  Integer objAsInt = Integer.class.cast(obj);
// do something with 'objAsInt'
}
```
У випадку використання дженериків:
```java
Object obj; // may be an integer
Class <T> type = // may be Integer.class
if (type.isInstance(obj)) {
  T objAsType = type.cast(obj);
  // do something with 'objAsType'
}
```
Оскільки в цьому випадку компілятор точно не знає, на який тип буде перетворюватися об’єкт, то таке перетворення називають *динамічним*
####Використання в Java 8
Досить часто під час роботи з `Optional<T>` та `Stream<T>` доводиться використовувати перетворення типів. Використання динамічного перетворення може зробити ваш код простіше.
Наприклад, з використанням статичного перетворення:
```java
Optional<?> obj; // may contain an Integer
Optional<Integer> objAsInt = obj
  .filter(obj -> obj instanceof Integer)
  .map(obj -> (Integer) obj);
```

З динамічним перетворенням типів:
```java
Optional<?> obj; // may contain an Integer
Optional<Integer> objAsInt = obj
.filter(Integer.class::isInstance)
.map(Integer.class::cast);

```
