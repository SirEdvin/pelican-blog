Title: Лямбда-вирази в Java, як та навіщо їх сералізувати
Slug: java-lambda-serailization
Lang: uk
Date: 08-02-2015
Category: Java
Tags: java, java8, lambdas
Status: published

![Java 8 lambda](https://technology.amis.nl/wp-content/uploads/2013/10/image18.png)

Механізм лямбда-виразів, що був реалізований у Java8, це сама така фіча, яка розділила код до неї та після (здається, таке саме може статися з Java9 та модульною системою, але у гірший бік). В Java 8 стало значно більше функціональних трюків, обробка великих масивів даних була значно спрощена і тепер займає куди менше місця. Але є багато різних питань стосовно використання лямбда-виразів, наприклад: наскільки раціонально їх використовувати постійно або настільки сильно ми втрачаємо у продуктивності при зміни звичайного циклу на `forEach()` з лямбда-виразом та інші. Більшість курсів (навіть [курс від Oracle](https://apexapps.oracle.com/pls/apex/f?p=44785:141:::NO::P141_PAGE_ID%2CP141_SECTION_ID:250%2C1807)) ігнорують ці важливі питання. В цьому пості буде описано одне з, можливо, найменш популярних, але не менш цікаве ніж інші питання:
> Як саме працює серіалізація лямбда-виразів в Java та як її можна використовувати?

###Як зробити лямбда-вираз серіалізуємим?

В першу чергу, важливо розуміти, що лямбда-функція в Java - це все ж таки *об’єкт* з одним нереалізованим методов (інші можна реалізувати через `default` у інтерфейсах (c) uthark) методом, а не *функція*. Тобто, ми можемо працювати з нею, як зі звичайним об’єктом. Іншими словами, лямбда-функція, яка реалізує серіалізуємий інтерфейс, буде серіалізуємою
```java
public interface SDoubleUnaryOperator extends DoubleUnaryOperator, Serializable {

}

public static void main(String[] args){
  SDoubleUnaryOperator t = t->t*t; //Лямбда-функція здатна до серіалізації
}
```

Іншим варіантом є використання нових фіч Java8, а саме такої, як приведення до перетину типів.
```java
public static void main(String[] args){
  DoubleUnaryOperator t = (DoubleUnaryOperator & Serializable)t->t*t; //Лямбда-функція здатна до серіалізації
}
```

В обох випадках, замість звичайної лямбда-функції, компілятор створить спеціально лямбда-функцію, [що буде здатна до серіалізації](https://docs.oracle.com/javase/8/docs/api/java/lang/invoke/SerializedLambda.html). Відповідно, далі з цією лямбдою можна буде працювати як з звичайним об’єктом, здатним до серіалізації, якщо вчасно зважати на ...

###Як відбувається серіаліація лямбда-функції?

Для розбору процесу серіалізації лямбда-функції був створений такий клас:
```java
import java.io.*;

public class Main {
    public static void main(String[] args) throws Exception {
        //saveLambda();
        loadLambda();
    }


    public static void saveLambda() throws IOException {
        Factory factory = new Factory(3);
        ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(new File("lambdas")));
        output.writeObject(factory.createLambda(4,new SerializableData(3.3,4)));
        output.writeObject(factory.createLambda(5,new SerializableData(5.3,-4)));
        output.writeObject(factory.createLambda(3,new SerializableData(10.3,+80e-5)));
        output.close();
    }


    public static void loadLambda() throws IOException, ClassNotFoundException {
        ObjectInputStream input = new ObjectInputStream(new FileInputStream(new File("lambdas")));
        DoubleToString operator1 = (DoubleToString) input.readObject();
        DoubleToString operator2 = (DoubleToString) input.readObject();
        DoubleToString operator3 = (DoubleToString) input.readObject();
        System.out.println(operator1.get(5));
        System.out.println(operator2.get(5));
        System.out.println(operator3.get(5));
    }
}
```

Увесь код знаходиться [тут](https://gist.github.com/SirEdvin/575bfe6f17ba371af735).

Під час серіалізації, лямбда-функція серіалізує усі змінні, які в ній використовуються, та зовнішній об’єкт, в якому вона реалізується, за умови, що вона використовує поля цього об’єкту. Відповідно, якщо щось з цих компонентів не буде заявлено як `Serializable`, то буде отримана `java.io.NotSerializableException`. Серіалізована лямбда-функція **не містить даних** про те, що саме вона має робити. Вона знає лише ідентифікатор відповідного їй спеціального класу.

Для мене це було розчарування. Адже, під час серіалізації лямбда-функції ми просто отримуємо список її параметрів та її ідентифікатор. Але після невеликих роздумів та експериментів я знайшов декілька корисних особливостей і зміг придумати один практичний приклад використання серіалізації лямбда-функцій.

####Особливості серіалізації лямбда-функцій

+ Відсутність дублікатів у серіалізації. Тобто якщо для декількох лямбда-функцій необхідний один і той самий об’єкт, він буде серіалізований лише один раз;
+ Мінімальна серіалізація. Серіалізуються лише ті об’єкти, які використовуються. Наприклад, якщо зовнішній клас не потрібний, то він не буде серіалізований;
+ Швидкість роботи лямбда-функції, здатної до серіалізації, та звичайної, майже не відрізняються.

###Практичне використання серіалізації лямбда-функцій

Єдиний корисний спосіб використання серіалізацій лямбда-виразів в Java, який я знайшов, це отримання лямбда-функцій з класу, який завантажується під час виконання програми. Справа в тому, що стандартних методів для отримання лямбда-функцій, які були реалізовані в цьому класу я не знайшов. Так що серіалізація може слугувати для передачі лямбда-функцій між програмами або використовувати їх для зовнішніх модулів.

Спочатку реалізуємо деякий зовнішній клас
```java
public class RemoteLambdaClass{
  public static void main(String[] args) throws Exception{
      DoubleUnaryOperator t1 = (DoubleUnaryOperator & Serializable) t->t*t;
      DoubleUnaryOperator t2 = (DoubleUnaryOperator & Serializable) t->t*t*t;
      ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(new File("lambdas")));
      output.writeObject(t1);
      output.writeObject(t2);
      output.close();
  }
}
```

Після його виконання буде створений файл `lambdas`, в якому і будуть знаходитися серіалізовані лямбда-функції. Далі створимо простий клас для читання (за умовами, зовнішній клас та файл лежать у директорії RemoteLambdaClass у корені проекту)
```java
public class Main {
    public static void main(String[] args) throws Exception {
        File f = new File("./RemoteLambdaClass/");
        //Додавання шляху у стандартний завантажувач
        URLClassLoader mainLoader = (URLClassLoader) Main.class.getClassLoader();
        Field classPathField = URLClassLoader.class.getDeclaredField("ucp");
        classPathField.setAccessible(true);
        URLClassPath urlClassPath = (URLClassPath) classPathField.get(mainLoader);
        urlClassPath.addURL(f.toPath().toUri().toURL());


        Class<?> clazz  = mainLoader.loadClass("RemoteLambdaClass");
        ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(new File(f,"lambdas")));
        DoubleUnaryOperator o1 = (DoubleUnaryOperator) inputStream.readObject();
        DoubleUnaryOperator o2 = (DoubleUnaryOperator) inputStream.readObject();
        System.out.println(o1.applyAsDouble(2));
        System.out.println(o2.applyAsDouble(2));
    }
}
```
Результати виконання:
```
4.0
8.0
```

####Висновки

Серіалізація лямбда-функція - це цікавий, але не практичний інструмент. Причиною для того, щоб робити лямбда-вирази здатними до серіалізації, швидше за все було бажання уникнути проблем з інтерфейсами, здатнами до серіалізації. Але, на мою думку, такому механізму все одно можна знайти ще кілька використань, які я пропустив.
