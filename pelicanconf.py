#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'SirEdvin'
SITENAME = "Заметки на стенах коробок"
SITEURL = 'http://localhost:8000'
MAIN_SITEURL = 'http://localhost:8000'

LOCALE = "ru_RU.UTF-8"
DEFAULT_PAGINATION = 4
DEFAULT_DATE = (2012, 3, 2, 14, 1, 1)

PATH = 'content'

TIMEZONE = 'Europe/Kiev'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = 'feeds/%s.atom.xml'

# Blogroll
LINKS = ()

# Social widget
SOCIAL = (
        ('github', 'https://github.com/siredvin/'),
        ('twitter', 'https://twitter.com/siredvin/'),
        ('bitbucket', 'https://bitbucket.org/siredvin'),
        ('linkedin','https://ua.linkedin.com/in/siredvin'),
         )

DEFAULT_PAGINATION = 10

RELATIVE_URLS = True

PLUGIN_PATHS = ["/pelican/env/plugins/"]
PLUGINS = ["sitemap", "bootstrapify"]

THEME = 'themes/aboutwilson/'

THEME_STATIC_DIR = 'theme'

DEFAULT_LANG = "ru"

DATE_FORMATS = {
    'ru': ('ru_RU.UTF-8','%a, %d %b %Y'),
}


DEFAULT_CATEGORY = 'Без категории'
DISQUS_SITENAME = "siredvin-blog"

OUTPUT_SOURCES = True

STATIC_PATHS = ['images', 'files', 'pdfs']

EXTRA_PATH_METADATA = {
    'files/.nojekyll': {
        'path': '.nojekyll',
        },
    }

SITEMAP = {
    'format': 'xml',
    'priorities': {
        'articles': 0.5,
        'indexes': 0.5,
        'pages': 0.5
    },
    'changefreqs': {
        'articles': 'monthly',
        'indexes': 'daily',
        'pages': 'monthly'
    }
}

DOCUTILS_SETTINGS = {
    'field_name_limit': 30,
    'option_limit': 30
}